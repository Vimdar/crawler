import os
from six.moves.configparser import RawConfigParser

MODEL_NAME = 'models'

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
config = RawConfigParser()
config.read('settings.conf')
config_section = 'database'
secret_section = 'secret'

SECRET_KEY = b'\xd0\xbd\xd0\xb0\xd0\xbf\xd0\xb8\xd1\x88\xd0\xb8\xd0\xba\xd1\x83\xd1\x80\xd0\xb4\xd0\xb0\xd1\x81\xd0\xb8\xd1\x85\xd0\xbe\xd0\xb4\xd0\xb8\xd0\xbc'.decode('utf8')
# SECRET_KEY = config.get(secret_section, 'SECRET_KEY')

DEBUG = True
INSTALLED_APPS = [
    'c_models.django_models.' + MODEL_NAME
]
# MIGRATION_MODULES = {MODEL_NAME: 'migrations'}
# ROOT_URLCONF = [
#     'urls'
# ]

DATABASES = {
    'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': config.get(config_section, 'DATABASE_NAME'),
            'USER': config.get(config_section, 'DATABASE_USER'),
            'PASSWORD': config.get(config_section, 'DATABASE_PASSWORD'),
            'HOST': config.get(config_section, 'DATABASE_HOST'),
            'PORT': '',
            # 'OPTIONS': :"{'options': '-c search_path=ipschema'}",

    }
}
